use criterion::{criterion_group, criterion_main, Criterion};
use jit_demo::cranelift;
use jit_demo::llvm;
use jit_demo::naive;

fn criterion_benchmark(c: &mut Criterion) {
    let mut group = c.benchmark_group("less_than_three");
    group.sample_size(10000);
    let f = naive::Comparer::new(3);
    let mut module = cranelift::default_jit_module();
    let cranelift_f = cranelift::generate_less_than_three(&mut module);
    let context = inkwell::context::Context::create();
    let module = context.create_module("less_than_three");
    let execution_engine = module
        .create_jit_execution_engine(inkwell::OptimizationLevel::Aggressive)
        .unwrap();
    let llvm_f = llvm::generate_less_than_three(&context, module, &execution_engine);

    for n in [0i32, 3, 4].iter() {
        group.bench_with_input(format!("baseline-{n}"), n, |b, &n| b.iter(|| n < 3));
        group.bench_with_input(format!("naive-{n}"), n, |b, &n| b.iter(|| f(n)));
        group.bench_with_input(format!("cranelift-{n}"), n, |b, &n| {
            b.iter(|| cranelift_f(n))
        });
        group.bench_with_input(format!("llvm-{n}"), n, |b, &n| {
            b.iter(|| unsafe { llvm_f(n) })
        });
    }
}

criterion_group!(benches, criterion_benchmark);
criterion_main!(benches);
