pub struct Comparer {
    n: i32,
}

impl Comparer {
    pub fn new(n: i32) -> Self {
        Comparer { n }
    }
}

impl FnOnce<(i32,)> for Comparer {
    type Output = bool;

    extern "rust-call" fn call_once(self, args: (i32,)) -> Self::Output {
        args.0 < self.n
    }
}

impl FnMut<(i32,)> for Comparer {
    extern "rust-call" fn call_mut(&mut self, args: (i32,)) -> Self::Output {
        args.0 < self.n
    }
}

impl Fn<(i32,)> for Comparer {
    extern "rust-call" fn call(&self, args: (i32,)) -> Self::Output {
        args.0 < self.n
    }
}

#[test]
fn test_comparer() {
    let c = Comparer { n: 3 };
    assert!(c(2));
    assert!(!c(3));
    assert!(!c(4));
}
