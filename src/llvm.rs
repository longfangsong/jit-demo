use inkwell::context::Context;
use inkwell::execution_engine::ExecutionEngine;
use inkwell::module::Module;
pub fn generate_less_than_three<'ctx>(
    context: &'ctx Context,
    module: Module<'ctx>,
    execution_engine: &ExecutionEngine<'ctx>,
) -> unsafe extern "C" fn(i32) -> bool {
    let builder = context.create_builder();
    let i8_type = context.i8_type();
    let i32_type = context.i32_type();
    let fn_type = i8_type.fn_type(&[i32_type.into()], false);
    let function = module.add_function("less_than_three", fn_type, None);
    let basic_block = context.append_basic_block(function, "entry");
    builder.position_at_end(basic_block);
    let x = function.get_nth_param(0).unwrap().into_int_value();
    let three = i32_type.const_int(3, false);

    let result = builder
        .build_int_compare(inkwell::IntPredicate::SLT, x, three, "SLE")
        .unwrap();
    builder.build_return(Some(&result)).unwrap();
    unsafe {
        execution_engine
            .get_function::<unsafe extern "C" fn(i32) -> bool>("less_than_three")
            .ok()
            .unwrap()
            .as_raw()
    }
}

#[test]
fn test_less_than_three() {
    let context = Context::create();
    let module = context.create_module("less_than_three");
    let execution_engine = module
        .create_jit_execution_engine(inkwell::OptimizationLevel::Aggressive)
        .unwrap();
    let f = generate_less_than_three(&context, module, &execution_engine);
    unsafe {
        assert!(f(1));
        assert!(!f(3));
        assert!(!f(4));
    }
}
