#![feature(unboxed_closures)]
#![feature(fn_traits)]

pub mod cranelift;
pub mod llvm;
pub mod naive;
