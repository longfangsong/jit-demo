use std::mem;

use cranelift::prelude::*;
use cranelift::{
    codegen::{
        entity::EntityRef,
        ir::{
            condcodes::IntCC,
            types::{self, I32},
            AbiParam, InstBuilder,
        },
        settings,
    },
    frontend::{FunctionBuilder, FunctionBuilderContext, Variable},
};
use cranelift_jit::{JITBuilder, JITModule};
use cranelift_module::{Linkage, Module};

pub fn default_jit_module() -> JITModule {
    let mut flag_builder = settings::builder();
    flag_builder.set("use_colocated_libcalls", "false").unwrap();
    flag_builder.set("is_pic", "false").unwrap();
    let isa_builder = cranelift_native::builder().unwrap_or_else(|msg| {
        panic!("host machine is not supported: {}", msg);
    });
    let isa = isa_builder
        .finish(settings::Flags::new(flag_builder))
        .unwrap();
    let builder = JITBuilder::with_isa(isa, cranelift_module::default_libcall_names());
    JITModule::new(builder)
}

pub fn generate_less_than_three(module: &mut JITModule) -> fn(i32) -> bool {
    let mut ctx = module.make_context();

    ctx.func.signature.params.push(AbiParam::new(types::I32));
    ctx.func.signature.returns.push(AbiParam::new(types::I8));
    let id = module
        .declare_function("less_than_three", Linkage::Export, &ctx.func.signature)
        .map_err(|e| e.to_string())
        .unwrap();

    let mut fn_builder_ctx = FunctionBuilderContext::new();

    let mut builder = FunctionBuilder::new(&mut ctx.func, &mut fn_builder_ctx);
    let block0 = builder.create_block();
    let x = Variable::new(0);
    builder.declare_var(x, I32);
    builder.append_block_params_for_function_params(block0);
    builder.switch_to_block(block0);
    builder.seal_block(block0);
    {
        let tmp = builder.block_params(block0)[0];
        builder.def_var(x, tmp);
    }
    let arg1 = builder.use_var(x);
    let result = builder.ins().icmp_imm(IntCC::SignedLessThan, arg1, 3);
    builder.ins().return_(&[result]);
    builder.finalize();

    module
        .define_function(id, &mut ctx)
        .map_err(|e| e.to_string())
        .unwrap();
    module.clear_context(&mut ctx);
    module.finalize_definitions().unwrap();
    // We can now retrieve a pointer to the machine code.
    let code = module.get_finalized_function(id);
    unsafe { mem::transmute::<_, fn(i32) -> bool>(code) }
}

#[test]
fn test_less_than_three() {
    let mut module = default_jit_module();
    let f = generate_less_than_three(&mut module);
    assert!(f(2));
    assert!(!f(3));
    assert!(!f(4));
}
